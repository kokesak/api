---
openapi: 3.0.0

info:
  title: Testing Farm Public API
  description: |
    Testing Farm API v0.1.

    Documentation: https://docs.testing-farm.io

    Created with ❤ by Red Hat.
  contact:
    name: Testing Farm Team
    email: tft@redhat.com
  license:
    name: Apache 2.0
    url: https://opensource.org/licenses/Apache-2.0
  version: "0.1"

servers:
  - url: https://api.dev.testing-farm.io/v0.1
    description: Development API endpoint

paths:
  #
  # Trigger endpoint
  #
  /requests:
    post:
      summary: Request a New Test
      operationId: requestsPost
      requestBody:
        description: Test request details
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/requestPostBody'
      responses:
        "201":
          description: Test request created
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/requestPostResponse'
        "400":
          description: Invalid request - schema validation failed
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/errorResponse"
        "403":
          description: Forbidden - api_key is invalid
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/errorResponse"
        "423":
          description: Locked - endpoint locked for the user by Testing Farm
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/errorResponse"
        default:
          description: Unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/errorResponse"

  #
  # Request retrieval endpoint
  #
  /requests/{request_id}:
    get:
      summary: Test Request Details
      operationId: requestsGet
      parameters:
        - name: request_id
          in: path
          description: Provide request_id you are interested in.
          required: true
          style: simple
          explode: false
          schema:
            $ref: '#/components/schemas/request_id'
      responses:
        "200":
          description: Request found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/requestGetResponse'
        "404":
          description: Request not found
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/errorResponse"
        default:
          description: Unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/errorResponse"

  #
  # Composes endpoint
  #
  /composes:
    get:
      summary: Composes Public Ranch
      operationId: composesGetPublic
      description: |
        Get list of composes for Public Ranch. Provided for backward compatibility.
        Use [/composes/{ranch}](#operation/composesGet) to get list of composes for a given ranch.
      responses:
        "200":
          description: Composes listing
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/composesGetResponse'
        default:
          description: Unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/errorResponse"

  /composes/{ranch}:
    get:
      summary: Composes
      operationId: composesGet
      description: |
        Get list of composes for a given ranch.
      parameters:
        - name: ranch
          in: path
          description: Provide ranch name you are interested in.
          required: true
          style: simple
          explode: false
          schema:
            $ref: '#/components/schemas/ranch'
      responses:
        "200":
          description: Composes listing
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/composesGetResponse'
        "404":
          description: Given ranch does not exist
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/errorResponse"
        default:
          description: Unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/errorResponse"

  #
  # About endpoint
  #
  /about:
    get:
      summary: About Testing Farm
      operationId: aboutGet
      responses:
        "200":
          description: Information about Testing Farm displayed.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/aboutGetResponse'
        "4xx":
          description: Unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/errorResponse"

#
# Schema components
#
components:
  schemas:
    #
    # request POST body
    #
    requestPostBody:
      type: object
      required:
        - api_key
        - test
      properties:
        api_key:
          $ref: '#/components/schemas/api_key'
        test:
          $ref: '#/components/schemas/test'
        environments:
          description: |
            List of test environments to provision.
          type: array
          items:
            $ref: '#/components/schemas/environment_requested'
        notification:
          $ref: '#/components/schemas/notification'
        settings:
          $ref: '#/components/schemas/settings'

    #
    # request test POST response
    #
    requestPostResponse:
      type: object
      properties:
        id:
          type: string
          description: The UUID is a unique identifier
          format: uuid
        created:
          $ref: '#/components/schemas/created'

    #
    # request GET response
    #
    requestGetResponse:
      type: object
      properties:
        id:
          $ref: '#/components/schemas/request_id'
        created:
          $ref: '#/components/schemas/created'
        updated:
          $ref: '#/components/schemas/updated'
        environments:
          description: |
            List of provisioned environments.
          type: array
          items:
            $ref: '#/components/schemas/environment_provisioned'
        state:
          $ref: '#/components/schemas/state'
        notes:
          description:
            Various notes produced by Testing Farm during testing. Each note contains a severity level and a message.
          type: array
          items:
            $ref: '#/components/schemas/note'
        result:
          $ref: '#/components/schemas/result'
        run:
          $ref: '#/components/schemas/run'

    #
    # composes GET response
    #
    composesGetResponse:
      type: object
      properties:
        composes:
          description:
            Composes supported by Testing Farm.
          type: array
          items:
            $ref: '#/components/schemas/compose'

    #
    # about GET response
    #
    aboutGetResponse:
      type: object
      properties:
        version:
          description:
            Display API endpoint application version.
          type: string
        release_notes:
          description:
            Version release notes.
          type: string

    #
    # common properties
    #
    request_id:
      description: |
        A unique identification of the request. Uses UUID format as defined in [RFC 4122](https://tools.ietf.org/html/rfc4122). Generated by Testing Farm.
      type: string
      format: uuid

    ranch:
      description: |
        A name of a Testing Farm Ranch. Currently available ranches are 'public' and 'redhat'.
      type: string
      enum:
        - public
        - redhat

    api_key:
      description: |
        An unique identifier used to authenticate a client.
      type: string
      example: "692f43c8636b1f1e2ab04c9881b4ab4e68088c9e"

    artifact:
      description: |
        Artifact which should be installed on the machine before running the tests. Please note that additional customization of artifact installation, like blacklisting of rpms, is supported with the `fmf` test type in the test metadata.
      type: object
      required:
        - id
        - type
      properties:
        id:
          $ref: '#/components/schemas/artifact.id'
        type:
          $ref: '#/components/schemas/artifact.type'
        packages:
          $ref: '#/components/schemas/artifact.packages'

    created:
      description: |
        Date/time of creation of the request in RFC 3339 format.
      type: string
      format: datetime
      example:
        2020-12-18T20:46:00.392227

    updated:
      description: |
        Date/time of last update of the request in RFC 3339 format.
      type: string
      format: datetime
      example:
        2020-12-18T20:46:00.392227

    #
    # requests GET properties
    #
    note:
      description: |
        Additional notes produced during testing.
      type: object
      properties:
        level:
          type: string
          enum:
            - info
            - warning
            - error
        message:
          type: string

    state:
      description: |
        State of processing of the request.
      type: string
      enum:
        - new
        - queued
        - running
        - complete
        - error

    result:
      description: |
        Result related properties.
      type: object
      properties:
        summary:
          $ref: '#/components/schemas/result.summary'
        overall:
          $ref: '#/components/schemas/result.overall'
        xunit:
          $ref: '#/components/schemas/result.xunit'

    run:
      description: |
        Details of the request run.
      type: object
      properties:
        console:
          $ref: '#/components/schemas/run.console'
        stages:
          description: |
            Stages of the test request with various details.
          type: array
          items:
            $ref: '#/components/schemas/run.stage'
        artifacts:
          $ref: '#/components/schemas/run.artifacts'

    #
    # requests POST properties
    #

    #
    # test properties
    #
    test:
      description: |
        Details about the test to run. Only one test type can be specified. If the user needs to run multiple tests, it should do it in separate requests.
      type: object
      properties:
        tmt:
          $ref: '#/components/schemas/test.tmt'
        sti:
          $ref: '#/components/schemas/test.sti'

    #
    # notification
    #
    notification:
      description: |
        Request update notification settings.
      type: object
      properties:
        webhook:
          type: object
          required:
            - url
          properties:
            url:
              description: |
                Post to given webhook URL in case of the request has changed. The purpose of the webhook is to inform our users about request changes and mitigates the need of periodic polling for request updates. The body of the request contains the `request_id` and an optional authentication `token`. In case of unsuccessful request, the request is retried few times.
              type: string
              format: url
            token:
              description: |
                Optional token to send in the body under key `token` when posting to the webhook URL. Provides means of authentication to the service accepting the webhook.
              type: string

    settings:
      description: |
        Various request settings or tweaks.
      type: object
      properties:
        worker:
          $ref: '#/components/schemas/settings.worker'
        pipeline:
          $ref: '#/components/schemas/settings.pipeline'

    settings.worker:
      description: |
        Worker settings or tweaks.
      type: object
      properties:
        image:
          type: string
          description: |
            Use given Testing Farm worker image instead of the one deployed in the specific ranch. Can be handy to test new releases of the worker image.
          example: quay.io/testing-farm/worker:1.0

    settings.pipeline:
      description: |
        Settings that affect the pipeline.
      type: object
      properties:
        timeout:
          description: |
            Timeout for the request in minutes. The default is 12h, i.e. 720 minutes.
          type: integer
          example: 720

    #
    # compose properties
    #
    compose:
      description: |
        Additional notes produced during testing.
      type: object
      properties:
        name:
          description: |
            Name of the compose.
          type: string
          example: Fedora-Rawhide

    #
    # artifact properties
    #
    artifact.id:
      description: |
        Unique identifier of the artifact. Value depends on the type of the artifact.

        For Public Ranch these artifacts are possible:

        * fedora-koji-build - use task ID of the koji build task, e.g. 43054146
        * fedora-copr-build - use the copr build-id:chroot-name, e.g. 1784470:fedora-32-x86_64
        * repository - baseurl of an RPM repository to install packages from, e.g. https://my.repo/repository
        * repository-file - an URL to a repository file which should be added to /etc/yum.repos.d, e.g. https://example.com/repository.repo

        For Red Hat Ranch these artifacts are possible:

        * fedora-copr-build - use the copr build-id:chroot-name, e.g. 1784470:fedora-32-x86_64
        * redhat-brew-build - use task ID of the Brew build task, e.g. 43924499

      type: string
      example: 43054146

    artifact.type:
      description: |
          Artifact type represents an artifact type which was built by a specific instance of a build system.

          Please see the `id` field documentation for details on artifact support in different ranches.

      type: string
      enum:
        - fedora-copr-build
        - fedora-koji-build
        - redhat-brew-build
        - repository
        - repository-file

    artifact.packages:
      description: |
        List of packages to install, if applicable to the artifact. By default all packages from the artifact are installed.
        Use any identifier which the package manager understands, e.g. openssh, openssh-8.2p1-73.f33, etc.
      type: array
      items:
        type: string
        example: openssh

    artifact.install:
      description: |
        Flag to indicate if the artifact should be installed. By default `true`.
      type: boolean
      default: true
      example: true

    #
    # environment properties
    #
    environment_requested:
      type: object
      required:
        - arch
      properties:
        arch:
          $ref: '#/components/schemas/environment.arch'
        os:
          $ref: '#/components/schemas/environment_requested.os'
        pool:
          $ref: '#/components/schemas/environment_requested.pool'
        variables:
          $ref: '#/components/schemas/environment.variables'
        secrets:
          $ref: '#/components/schemas/environment.secrets'
        artifacts:
          $ref: '#/components/schemas/environment.artifacts'
        hardware:
          $ref: '#/components/schemas/environment.hardware'
        settings:
          $ref: '#/components/schemas/environment.settings'
        tmt:
          $ref: '#/components/schemas/environment.tmt'

    environment_provisioned:
      type: object
      required:
        - arch
        - os
        - pool
      properties:
        arch:
          $ref: '#/components/schemas/environment.arch'
        os:
          $ref: '#/components/schemas/environment_provisioned.os'
        pool:
          $ref: '#/components/schemas/environment_provisioned.pool'
        variables:
          $ref: '#/components/schemas/environment.variables'

    environment.arch:
      description: |
        Force the given architecture of the test environment to provision.
      type: string
      example: x86_64

    environment_provisioned.pool:
      description: |
        Name of the infrastructure pool which was used for testing.
      type: string
      example: aws

    environment_requested.pool:
      description: |
        Name of the infrastructure pool to use. If not chosen, Testing Farm will choose the most suitable pool.
      type: string
      example: aws-pool-01

    environment_requested.os:
      description: |
        Identifies the operating system used for the test environment.
      type: object
      required:
        - compose
      properties:
        compose:
          $ref: '#/components/schemas/environment_requested.os.compose'

    environment_requested.os.compose:
      type: string
      description: |
        Specify the compose of the operating system to provision.
        Let Testing Farm choose the best infrastructure pool for execution.
        Both specific IDs and symbolic names can be specified.
        Symbolic names are translated to specific IDs. List of symbolic composes are:
        https://docs.testing-farm.io/general/0.1/test-environment.html#_composes
        If you omit this field, the test execution will happen in a container defined in the `tmt` plan.
      example: Fedora-Rawhide

    environment_provisioned.os:
      description: |
        Identifies the operating system used for the test environment.
      type: object
      required:
        - compose
      properties:
        compose:
          $ref: '#/components/schemas/environment_provisioned.os.compose'

    environment_provisioned.os.compose:
      type: string
      description: |
        Compose ID reflects the real VM image, compose or container image used for testing.
      example: Fedora-32-20200414.0

    environment.artifacts:
      type: array
      items:
        $ref: '#/components/schemas/artifact'
      description: |
        Additional artifacts to install in the test environment.

    environment.hardware:
      type: object
      description: |
        Test environment hardware specification. See https://tmt.readthedocs.io/en/stable/spec/plans.html#hardware for details.
      example:
        hostname: my.machine.com
        disk:
          size: ">=100GB"

    environment.os:
      description: |
        Identifies the operating system used for the test environment.
      type: object
      required:
        - compose
      properties:
        compose:
          $ref: '#/components/schemas/environment_requested.os.compose'

    environment.variables:
      description: |
        A mapping of environment variable names to values, which will be exported in the test environment.
      type: object
      example:
        FOO: some-value
        BOO: other-value

    environment.secrets:
      description: |
        A mapping of secret environment variable names to values, which will be exported in the test environment.
        Testing Farm will hide the values from standard output of the tests and logs. If the tests generate their
        own log files, they are responsible for confidentiality of the secrets.
      type: object
      example:
        FOO: some-value
        BOO: other-value

    environment.tmt.context:
      description: |
        A mapping of tmt context variable names to values. For more information about tmt context see [documentation](https://tmt.readthedocs.io/en/latest/spec/context.html).
      type: object
      example:
        distro: fedora-33
        arch: x86_64
        trigger: code

    environment.tmt.environment:
      description: |
        A mapping of environment variables exposed to `tmt`.
        Used to configure various `tmt` plugins with possibly sensitive configuration.
        For example for the `polarion` report plugin, etc.
      type: object
      example:
        POLARION_URL: https://polarion.example.com/polarion
        POLARION_REPO: https://polarion.example.com/repo
        POLARION_USERNAME: polarion
        POLARION_PASSWORD: polarion
        POLARION_PROJECT: AutoSD

    environment.tmt:
      description: |
        Special environment settings for `tmt` tool.
      type: object
      properties:
        context:
          $ref: '#/components/schemas/environment.tmt.context'
        environment:
          $ref: '#/components/schemas/environment.tmt.environment'

    environment.settings:
      description: |
        Various environment settings or tweaks.
      type: object
      properties:
        pipeline:
          $ref: '#/components/schemas/environment.settings.pipeline'
        provisioning:
          $ref: '#/components/schemas/environment.settings.provisioning'

    environment.settings.pipeline:
      description: |
        Pipeline settings or tweaks.
      type: object
      properties:
        skip_guest_setup:
          type: boolean
          default: false
          description: |
            Skip guest setup playbooks. Can be handy to test guest setup playbooks.
          example: true

    environment.settings.provisioning:
      description: |
        Guest settings or tweaks to be applied during provisioning.
      type: object
      properties:
        post_install_script:
          type: string
          description: |
            Pass a specific post-install-script to be used during guest provisioning.
          example: "#!/bin/sh\nsudo sed -i 's/.*ssh-rsa/ssh-rsa/' /root/.ssh/authorized_keys\n"
        tags:
          type: object
          description: |
            Dictionary of tags for labeling cloud resources provisioned for the test environment. Use `BusinessUnit: team_name` to tag resources for Testing Farm's cost reporting. The team name can be any string, but it is advised to use subsystem team name if applicable, e.g. `sst_cs_apps_rhel`, etc.
          example:
            BusinessUnit: sst_cs_apps_rhel

    #
    # test properties
    #
    test.tmt:
      description: |
        Run tests defined via `tmt`.
      required:
        - url
      type: object
      properties:
        url:
          type: string
          description: |
            Git repository containing the metadata tree. Use any format acceptable by the git clone command.
          example: https://github.com/teemtee/tmt
        ref:
          type: string
          description: |
            Branch, tag or commit specifying the desired git revision. This is used to perform a git checkout in the repository.
          default: master
          example: "0.10"
        path:
          type: string
          description: |
            Path to the metadata tree root. Should be relative to the git repository root provided in the `url` parameter.
          default: .
          example: /examples/wget
        name:
          type: string
          description: |
            Run plans by the given name.
            Can be a regular expression.
            Passed as a positional argument to the `tmt plan ls` command.
            By default all plans are run.
          example: example-plan
        plan_filter:
          type: string
          description: |
            Filter tmt plans.
            Can be a regular expression.
            Passed as a `--filter` option to the `tmt plan ls` command.
            By default `enabled: true` filter is applied.
            See https://tmt.readthedocs.io/en/stable/stories/features.html#filter for more information.
          example: "tag: tier1"
        settings:
          type: object
          properties:
            recognize-errors:
              type: boolean
              description: |
                By default we map `error` state from `tmt` to `failed` overall result of the request.
                If you want to recognize `error` outcome from tmt as an `error` overall state of the request, enable this setting.
                A real life usage of this option is in Fedora CI, where for generic tests it makes more sense to recognize these errors as they mean that the generic test is broken and it is the responsibility of Fedora CI to fix it. For tests defined by the users - i.e. generic tests, fixing these tests is the responsibility of the user and Fedora CI does not want them to mark them as `error`.
              default: false
              example: true

    test.sti:
      description: |
        Run tests defined via STI.
      required:
        - url
        - ref
      type: object
      properties:
        url:
          type: string
          description: |
            Git repository containing the STI tests. Use any format acceptable by the git clone command.
          example: https://src.osci.redhat.com/rpms/cpio
        ref:
          type: string
          description: |
            Branch, tag or commit specifying the desired git revision. This is used to perform a git checkout in the repository.
          example: "f31"
        playbooks:
          type: array
          items:
            type: string
            example: tests/tests*.yml
          description: |
            Playbooks to run from the given repositories. Globbing is supported. By default standard `tests/tests*.yml` is used.
          default:
            - tests/tests*.yml
        extra_variables:
          description: |
            A mapping of Ansible extra variable names to values, which will be passed to `ansible-playbook`.
          type: object
          example:
            foo: some-foo-value
            boo: some-boo-value

    test.script:
      description: |
        Run given scripts from the given GIT repository in the default shell.
      required:
        - url
        - ref
        - scripts
      type: object
      properties:
        url:
          type: string
          description: |
            Git repository containing the scripts. Use any format acceptable by the git clone command.
          example: https://src.osci.redhat.com/rpms/cpio
        ref:
          type: string
          description: |
            Branch, tag or commit specifying the desired git revision. This is used to perform a git checkout in the repository.
          example: "f31"
        scripts:
          type: array
          items:
            type: string
            example: tests/test.sh
          description: |
            Scripts to run. Script is a command executed from the root of the cloned directory om the given test environment. More commands can be specified. Each command is asserted on return code 0.

    #
    # result properties
    #
    result.summary:
      description: |
        Human readable summary of the test request. In case of error state contains the error message. In case of skipped results, contains the reason of the skipping. In case of passed or failed results in contains a human readable interpretation of the test results, e.g. 1 plans from 3 failed.
      type: string
      example: Installation of the artifact failed. See https://artifacts.testing-farm.io/4b1b77bd-8861-40d1-bda1-849728b49aaa/stages/artifact-installation/log.txt for more information.

    result.overall:
      description: |
        Overall result of testing the request. Special value `unknown` means that result could not be determined. The overall result `error` was added because some test frameworks recognize this state themselves, e.g. tmt.
      type: string
      enum:
        - passed
        - failed
        - skipped
        - error
        - unknown

    result.xunit:
      description: |
        A URL link to test results in XUnit format.
      type: string
      example: |
        https://artifacts.testing-farm.io/4b1b77bd-8861-40d1-bda1-849728b49aaa/xunit.xml

    #
    # run properties
    #
    run.console:
      description: |
        URL of a plain-text log from Testing Farm, which can be followed for progress.
      type: string
      example: https://console.testing-farm.io/baa1ba84-f62c-40c3-b3b9-fa0ce3c478a4

    run.stage:
      description: |
        Details of the stage in Testing Farm test run.
      type: object
      properties:
        name:
          description: |
            Name of the stage.
          type: string
          example: pre-artifact-installation
        notes:
          description: |
            Notes produced by Testing Farm related to this concrete stage.
          type: array
          items:
            $ref: '#/components/schemas/note'
        result:
          description: |
            Result of the test stage.
          type: string
          enum:
            - passed
            - failed
            - skipped
        log:
          description: |
            URL to a stage specific log. Can point also to an artifact directory with various logs.
          type: string
          format: uri
          example: https://artifacts.testing-farm.io/baa1ba84-f62c-40c3-b3b9-fa0ce3c478a4/stages/artifact-installation/

    run.artifacts:
      description: |
        URL to the root of produced artifacts from the test request.
      type: string
      format: uri
      example: https://artifacts.testing-farm.io/baa1ba84-f62c-40c3-b3b9-fa0ce3c478a4/

    #
    # Error response for all endpoints
    #
    errorResponse:
      type: object
      required:
        - code
        - message
      properties:
        code:
          description: |
            HTTP Error code.
          type: integer
          format: int32
        message:
          description: |
            Error message.
          type: string
